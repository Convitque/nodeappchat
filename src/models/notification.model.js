import mongoose from "mongoose"

let Schema = mongoose.Schema;

let NotificationSchema = Schema({
    sender: {
        id: String,
        username: String,
        avatar: {type: String, default: "avatar-default.jpg"}
    },
    receiver: {
        id: String,
        username: String,
        avatar: {type: String, default: "avatar-default.jpg"}
    },
    type: String,
    content: String,
    isRead: {type: Boolean, default: false},
    createdAt: {type: Number, default: Date.now},
})