import mongoose from "mongoose"

let Schema = mongoose.Schema;

let MessageSchema = Schema({
    sender: {
        id: String,
        username: String,
        avatar: {type: String, default: "avatar-default.jpg"}
    },
    receiver: {
        id: String,
        username: String,
        avatar: {type: String, default: "avatar-default.jpg"}
    },
    text: String,
    file: {data: Buffer, contentType: String},
    createdAt: {type: Number, default: Date.now},
	updatedAt: {type: Number, default: null},
	deletedAt: {type: Number, default: null}
})

module.exports = mongoose.model("message", MessageSchema);