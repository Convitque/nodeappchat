import {validationResult} from "express-validator"

let getLoginRegister = (req, res) => {
    res.render("auth/master", {
        errors: req.flash("errors")
    })
}

let postRegister = (req, res) => {
    //console.log(req.body);
    let errorsResult = [];

    let validationErrors = validationResult(req);
    if(!validationErrors.isEmpty()) {
        let errors = Object.values(validationErrors.mapped())

        errors.forEach(element => {
            errorsResult.push(element.msg)
        });
        req.flash("errors", errorsResult)
        return  res.redirect('/login');
    }

    console.log(errorsResult);
}

module.exports = {
    getLoginRegister: getLoginRegister,
    postRegister:postRegister
}