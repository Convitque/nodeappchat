import express from "express"
const app = express();

require('dotenv').config()

console.log(process.env.APP_HOSTNAME)

import ConfigViewEngine from "./config/viewEngine"
import ContactModel  from "./models/contact.model"
import ConncetDB from "./config/connect"
import initRoutes from './routes/web';
import bodyParser from 'body-parser'
import connectFlash from 'connect-flash'
import configSession from'./config/session'
// Connect to mongodb
ConncetDB();

// Config session
configSession(app);
// Config view engine
ConfigViewEngine(app);

// body parser

app.use(bodyParser.urlencoded({extended: true}));

// flash message
app.use(connectFlash);
// Init routes
initRoutes(app);

app.listen(process.env.APP_PORT,process.env.APP_HOSTNAME ,() => {
    console.log(`Server started on port ${process.env.APP_PORT}`);
});
