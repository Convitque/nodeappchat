import express from "express"
import {home, auth} from "../controllers/indexController"
import {authValid} from "../validation/index"


let router = express.Router();



/**
 * Init routes
 * @param app from exacty express module
 */

let initRoutes = (app) => {
    router.get('/', home);
    router.get('/login',auth.getLoginRegister);
    router.post('/register',authValid.register, auth.postRegister);
    return app.use("/", router);
}

module.exports = initRoutes;