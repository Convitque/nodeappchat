import session from "express-session"
import connectMongo from "connect-mongo"
/**
 * this variable to save session
 */
let DB_CONNECTION = "mongodb";
let DB_HOST = "localhost";
let DB_PORT = 27017;
let DB_NAME = "awesome_chat";
let DB_USERNAME = "";
let DB_PASSWORD = "";



// console.log(`${process.env.DB_CONNECTION}://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`);

let MongoStore = connectMongo(session);
let sessionStore = new MongoStore({
    // url: `${DB_CONNECTION}://${DB_HOST}:${DB_PORT}/${DB_NAME}`,
    url: `${process.env.DB_CONNECTION}://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`,
    autoReconnect: true,
    autoRemove: "native"
})

/**
 * session for app
 * @param app from exactly  express module
 */

let configSession = (app) => {
    app.use(session({
        key: "express.sid",
        secret: "Secret",
        store: sessionStore,
        resave: true,
        saveUninitialized: false,
        cookie: {
            maxAge:1000*60*60*24,
        }
    }));
}

module.exports = configSession;