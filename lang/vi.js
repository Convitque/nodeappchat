export const transValidation = {
    email_incorrect: "Email phải có dạng example@gmail.com",
    gender_incorrect: "Giới tính sai vui lòng chọn lại",
    password_incorrect: "Mật khẩu chứa ít nhất 8 ký tự bao gồm chữ hoa, chữ thường, số và ký tự đặc biệt.",
    password_confirmation_incorrect: "Nhập lại mật khẩu chưa chính xác"
}